//Activity
let students = [];

function addStudent(nameStudent){
	students.push(nameStudent);
	return (`${nameStudent} was added to the student's list`);
}

function countStudents(){
	return (`There are a total of ${students.length} students enrolled.`);
}
function printStudents(){
	students.sort();
	students.forEach(
		function(studentList){
			console.log (studentList);
	})
}

let foundStudents=[];

function findStudent(keyword) {
	for (let i = 0; i < students.length; i++) {
		if (students[i].toLowerCase() == keyword.toLowerCase() || students[i][0].toLowerCase() == keyword.toLowerCase()){
			foundStudents.push(students[i])
		}		
	}
	if (foundStudents.length == 1){
			return (`${foundStudents[0]} is an enrolee`)
	}
	else if (foundStudents.length > 1){
		return (`${foundStudents.join(', ')} are enrolees`)
	}
	else
		return (`No student found with the name ${keyword}`);
}



// Stretch Goals

//
function addSection(section){

let mapSection = students.map(
	function(studyante){
		return (studyante += ` - section ${section.toUpperCase()}`)
	}
)
return mapSection
}

//
function firstCapital(){

	let mapCapital = students.map(
		function(caps){
			return (caps.replace(caps[0],caps[0].toUpperCase()))
		}
	)
	return mapCapital
}

//
function removeStudent(tanggal){
	for (let i = 0; i < students.length; i++) {
		if (students[i] ==  tanggal){
			let index = students.indexOf(tanggal);
			students.splice(index,1);
			return (`${tanggal} was removed from the student's list`)
		}	
	}
}
